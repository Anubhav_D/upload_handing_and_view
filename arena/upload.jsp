<%@ page import="javax.servlet.http.*" %>
<%@ page import="javax.servlet.*" %>
<%@ page session="false" %>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap.min.css" /> 
</head>
<%
HttpSession hs=request.getSession(false);
if(hs==null){
	%>
	<form>
	<input type="hidden" name="status" value="You need to Login first!"/>
	</form>
	<jsp:forward page="Login_page.jsp?status=login&path=upload"/>
	<%
}
else{
	System.out.println(hs.getAttribute("name"));
}



%>
<body>
<div class="container">
<div class="pageheader" style="background-color: #ffffff">
<a href="/arena/Homepage.jsp">
<img src="${pageContext.request.contextPath}/logo.PNG" /></a><img src="${pageContext.request.contextPath}/uploadPageTitle.PNG"/>
</div>
<hr>
<div class="container"  style="background-color:#d3d1d1;float:left;border-radius: 5px">
<form action="/arena/UploadServe" method="POST" enctype="multipart/form-data">
<div class="form-group">
<label for="title">Title</label>
<input type="text" class="form-control" id="title" name="title" placeholder="Enter Title"><sub style="color:red">${Title}</sub>
</div>
<div class="form-group">
    <label for="File">Video</label>
    <input type="file" class="form-control" id="File" name="File2" accept="video/mp4"><sub style="color:red">${Video}</sub>
  </div>
  <div class="form-group">
<label for="thumb">Upload Poster</label>
<input type="file" class="form-control" id="thumb" name="thumbnail" accept="image/jpeg,image/png" ><sub style="color:red">${Poster}</sub>
  </div>
<div class="form-group">
<label for="description">Description</label>
<textarea class="form-control" id="description" name="description" rows=3 placeholder="Enter Description"></textarea><sub style="color:red">${Description}</sub>
<small class="form-text muted-text">A minimum 40 characters description required</small> 

</div>
 <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  </div>
</body>
</html>