import java.io.File;
import java.io.IOException;
import java.util.List;
import java.sql.*;
import javax.servlet.*;
//import classes.Video;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@WebServlet("/UploadServe")

public class UploadServlet extends HttpServlet{
	

	protected void doPost(HttpServletRequest req,HttpServletResponse res)
	throws ServletException,IOException{
				System.out.println(req.getContextPath());
			
		
			try{

				List<FileItem> multiparts=new ServletFileUpload(new DiskFileItemFactory()).parseRequest(req);
				int count=0;
				int inner_count=0;
				boolean redirect_flag=false;
				String data[]=new String[2];
				ServletContext context=getServletContext();  
				for(FileItem item:multiparts){
					if(!item.isFormField()){
						if(item.getName().isEmpty()){
							if(inner_count==0){
								context.setAttribute("Video","Please Select an video file");
								System.out.println("video-set");
							}
							else{
								context.setAttribute("Poster","Please Provide an poster for your video file");
							}
							redirect_flag=true;
							inner_count++;
						}
						else
						{
							//System.out.println("in");
							if(inner_count==0){
								context.setAttribute("Video","");
							}
							else{
								context.setAttribute("Poster","");
							}
						
					}
					inner_count++;
					}
					else{
					data[count]=item.getString();
					
					if(count==0){
						
					if(data[count].isEmpty()){
						context.setAttribute("Title","Please provide an video title");
						redirect_flag=true;
					}
					else{
						context.setAttribute("Title","");
					}
					}
					if(count==1){
					if(data[count].isEmpty()){
						context.setAttribute("Description","Please provide an Description");
						redirect_flag=true;
					}
					else{
						context.setAttribute("Description","");
					}
						}
					count++;
					}
					
				}

				if(redirect_flag){
					RequestDispatcher rd=req.getRequestDispatcher("upload.jsp");
					rd.forward(req,res);
				}
				/*
				as request can be processed only once Hence meta-data about the video is extracted in data** named String array	

				*/										
				//for(int i=0;i<data.length;i++){
				//	System.out.println(data[i]);
				//}

				
				else{
				int row=Utility.get_new_video_id();
				
				for(FileItem item:multiparts){
					if(!item.isFormField()){
						String type=Utility.extension_finder(item.getName());
						if(type.equals("video")){
								String UPLOAD_DIRECTORY="C:\\Users\\Anubhav\\Desktop\\apache-tomcat-9.0.0.M21\\webapps\\arena\\videos";
						//String name=new File(item.getName()).getName();
						item.write(new File(UPLOAD_DIRECTORY+File.separator+row+".mp4"));
					}
					else if(type.equals("image")){
							String UPLOAD_DIRECTORY="C:\\Users\\Anubhav\\Desktop\\apache-tomcat-9.0.0.M21\\webapps\\arena\\images";
						//String name=new File(item.getName()).getName();
						item.write(new File(UPLOAD_DIRECTORY+File.separator+row+".jpg"));
					}
					else{
						System.out.println(type);
					}
					}
				}
				//----------------------------------------------Database insertion Code Started--------------------------------------------------------
				Video video=new Video();
				video.video_id=row;
				HttpSession hs=req.getSession();
				video.video_author=(String)hs.getAttribute("name");
				video.video_duration="5:00";
				video.video_title=data[0];
				video.video_description=data[1];
				System.out.println(row);
				boolean flag=Utility.upload_new_video_meta_data(video);
				//-------------------------------------------Database insertion code completed----------------------------------------------------
				if(flag){
				RequestDispatcher rd=req.getRequestDispatcher("/uploadConfirmation.jsp");
				rd.forward(req,res);
				}
				else{
					System.out.println("Sorry,Some Error Occured");
				}
			}
			}
			catch(Exception e){

			System.out.println(e);


		}
	}


	}