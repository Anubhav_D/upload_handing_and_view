import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import javax.servlet.annotation.*;
@WebServlet("/register")
public class Registration_authenticator extends HttpServlet{

	public void doPost(HttpServletRequest req,HttpServletResponse res){
		try{
			Connection con=Database_connector.connect_to_database();
			Statement st=con.createStatement();
			String username=req.getParameter("username");
			String email=req.getParameter("email");
			String password=req.getParameter("password");
			ResultSet rs=st.executeQuery("Select count(*) from users_info where email=\'"+email+"\'");
			rs.next();
			int result=rs.getInt(1);
			if(result>0){

				req.setAttribute("status","This email is already registered!");
				con.close();
				RequestDispatcher rd=req.getRequestDispatcher("Register_page.jsp");
				rd.forward(req,res);
			}
			else{
				System.out.println("inside else");
				ResultSet rs2=st.executeQuery("select count(*) from users_info");
				rs2.next();
				int row=rs2.getInt(1);
				PreparedStatement pst=con.prepareStatement("insert into users_info values(?,?,?)");
				pst.setInt(1,row+1);
				pst.setString(2,username);
				pst.setString(3,email);
				//pst.setString(4,String.valueOf(System.currentTimeMillis()));
				int result1=pst.executeUpdate();
				if(result1!=1){
					req.setAttribute("status","We are facing some technical difficulties please try after some time...");
					con.close();
				RequestDispatcher rd=req.getRequestDispatcher("Register_page.jsp");
				rd.forward(req,res);
				System.out.println("middle if");
				}

				else{
					System.out.println("middle else");
					PreparedStatement pst2=con.prepareStatement("insert into users values(?,?,?)");
					pst2.setInt(1,row+1);
					pst2.setString(2,email);
					pst2.setString(3,password);
					int result2=pst2.executeUpdate();
					if(result2!=1){
					req.setAttribute("status","We are facing some technical difficulties please try after some time...");
					con.close();
				RequestDispatcher rd=req.getRequestDispatcher("Register_page.jsp");
				rd.forward(req,res);
				}
				else{
					con.close();
					RequestDispatcher rd=req.getRequestDispatcher("Login_page.jsp");
					System.out.println("going outside");
					rd.forward(req,res);
				}
				}
			}
			
		}
		catch(Exception e){

			System.out.println(e+" in Registration_authenticator");
		}
		
	}
}