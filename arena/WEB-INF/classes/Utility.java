
import java.sql.*;
import java.util.regex.*;
public class Utility{
public static int get_new_video_id(){
				try{
				Class.forName("oracle.jdbc.OracleDriver");
				
				Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","SYSTEM","root");
				
				Statement st=con.createStatement();
				ResultSet rs=st.executeQuery("SELECT max(video_id) from video_info");
				rs.next();
				int row=rs.getInt(1);
				
				con.close();
				return row+1;
				}
				catch(Exception e){
					System.out.println(e);
				}
				return -1;
}


public static boolean upload_new_video_meta_data(Video video){
				try{
				Class.forName("oracle.jdbc.OracleDriver");
				
				Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","SYSTEM","root");
				PreparedStatement pst=con.prepareStatement("insert into video_info values(?,?,?,?,?)");
				pst.setInt(1,video.video_id);
				pst.setString(2,video.video_author);
				pst.setString(3,video.video_duration);
				pst.setString(4,video.video_title);//inserting video_title
				pst.setString(5,video.video_description);//inserting video_description
				int flag=pst.executeUpdate();
				Statement st=con.createStatement();
				int row=st.executeUpdate("insert into video_views values("+video.video_id+",0)");
				con.close();
				if(flag==1)
					return true;
				else
					return false;
			}
			catch(Exception e){
				System.out.println(e);
			}
			return false;
}
public static String extension_finder(String name){
	Pattern p=Pattern.compile("^[a-zA-Z0-9!@#$&()\\[\\]\\{\\}\\_\\|\\<\\>\\%\\'\\-`.+,/\" ]*.mp4");
	Matcher m=p.matcher(name);
	if(m.find() && m.group().equals(name)){
		return "video";
	}
	else{
		Pattern p2=Pattern.compile("^[a-zA-Z0-9!@#$&()\\-`.+,/\" ]*.[jpg|png|jpeg|PNG]");
		Matcher m2=p2.matcher(name);
		if(m2.find() && m2.group().equals(name)){
			return "image";
		}
		else{
			return "none";
		}
	}
}


}