package classes;
import classes.Video;
import java.sql.*;
public class Homepage_Utility{
	public Video[] get_data() throws Exception{
		
		System.out.println("In");
		Class.forName("oracle.jdbc.OracleDriver");
		Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","SYSTEM","root");
		Statement st=con.createStatement();
		ResultSet row=st.executeQuery("Select count(*) from video_info");
		row.next();
		int length=row.getInt(1);
		System.out.println(length);
		st.close();
		Statement st2=con.createStatement();
		Video array[]=new Video[length];
		//System.out.println(array[0]);
		ResultSet rs=st2.executeQuery("Select * from video_info where video_id >=1");
		int i=0;
		while(rs.next()){
			array[i]=new Video();
			//System.out.println(rs.getInt(1)+rs.getString(2)+rs.getString(3)+rs.getString(4));
			array[i].video_id=rs.getInt(1);
			//System.out.println(rs.getString(2));
			array[i].video_author=rs.getString(2);
			array[i].video_duration=rs.getString(3);
			array[i].video_title=rs.getString(4);
			array[i].video_description=rs.getString(5).substring(0,Math.min(rs.getString(5).length(),30));
			i++;
		}
		System.out.println("out");
		con.close();
		return array;
	
	}

	public static int get_views(int id){
		try{
		Class.forName("oracle.jdbc.OracleDriver");
		Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","SYSTEM","root");
		Statement st=con.createStatement();
		ResultSet rs=st.executeQuery("select * from video_views where video_id="+id);
		rs.next();
		int views=rs.getInt(2);
		System.out.println(views);
		//st.close();
		con.close();
		return views+1;
	}
catch(Exception e){
	System.out.println(e);
	}
	return 1;
}

public static void set_views(int views,int id){
	try{
	Class.forName("oracle.jdbc.OracleDriver");
	Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","SYSTEM","root");
	Statement st=con.createStatement();
		System.out.println(1);
		String query="update video_views set views="+(views)+" where video_id="+id;
		System.out.println(query);
		int row=st.executeUpdate(query);
		System.out.println(2);
		if(row==1)
			System.out.println("row updated");
		else
			System.out.println("nope");
}
catch(Exception e){
	System.out.println(e);

}
}

public static Video get_video_info(int id){
	try{
	Class.forName("oracle.jdbc.OracleDriver");
	Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","SYSTEM","root");
	Statement st=con.createStatement();
	Video video=new Video();
	ResultSet rs=st.executeQuery("select * from video_info where video_id="+id);
	rs.next();
	video.video_id=rs.getInt(1);
	video.video_author=rs.getString(2);
	video.video_duration=rs.getString(3);
	video.video_title=rs.getString(4);
	video.video_description=rs.getString(5);
	return video;

}
catch(Exception e){
	System.out.println(e);

}
return null;
}
}