<%@ page session="false" %>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap.min.css" /> 
<style>
body {
	background: #eee !important;	
}

.wrapper {	
	margin-top: 80px;
  margin-bottom: 80px;
}

.form-signin {
  max-width: 380px;
  padding: 15px 35px 45px;
  margin: 0 auto;
  background-color: #fff;
  border: 1px solid rgba(0,0,0,0.1);  

  .form-signin-heading,
	.checkbox {
	  margin-bottom: 30px;
	}

	.checkbox {
	  font-weight: normal;
	}

	.form-control {
	  position: relative;
	  font-size: 16px;
	  height: auto;
	  padding: 10px;
		@include box-sizing(border-box);

		&:focus {
		  z-index: 2;
		}
	}

	input[type="text"] {
	  margin-bottom: -1px;
	  border-bottom-left-radius: 0;
	  border-bottom-right-radius: 0;
	}

	input[type="password"] {
	  margin-bottom: 20px;
	  border-top-left-radius: 0;
	  border-top-right-radius: 0;
	}
}
</style>
</head>
<body style="background-color:#d3d1d1">
<div class="container-fluid">
<div class="pageheader" style="padding:10px 10px 10px 10px;background-color: #ffffff;border-radius: 5px" >
<a href="/arena/Homepage.jsp">
<img src="${pageContext.request.contextPath}/logo.PNG" /></a>
</div>
</div>
<%
String name="previous name";
String status=(String)request.getAttribute("status");
if(status==null){
	status="";
}
%>
<div class="wrapper">
 <center><div style="color: red"><%= status%></div></center>
    <form class="form-signin" method="POST" action="/arena/register">       
      <h2 class="form-signin-heading">Registration</h2>
      <input type="text" class="form-control" name="username" placeholder="Username" required="" autofocus="" /><br>
      <input type="text" class="form-control" name="email" placeholder="email" required="" /><br>
      <input type="password" class="form-control" name="password" placeholder="Password" required=""/> <br>     
      <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>   
    </form>
  </div>


</body>
</html>