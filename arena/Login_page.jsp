<%@ page session="false" %>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap.min.css" /> 
<style>
body {
	background: #eee !important;	
}

.wrapper {	
	margin-top: 80px;
  margin-bottom: 80px;
}

.form-signin {
  max-width: 380px;
  padding: 15px 35px 45px;
  margin: 0 auto;
  background-color: #fff;
  border: 1px solid rgba(0,0,0,0.1);  

  .form-signin-heading,
	.checkbox {
	  margin-bottom: 30px;
	}

	.checkbox {
	  font-weight: normal;
	}

	.form-control {
	  position: relative;
	  font-size: 16px;
	  height: auto;
	  padding: 10px;
		@include box-sizing(border-box);

		&:focus {
		  z-index: 2;
		}
	}

	input[type="text"] {
	  margin-bottom: -1px;
	  border-bottom-left-radius: 0;
	  border-bottom-right-radius: 0;
	}

	input[type="password"] {
	  margin-bottom: 20px;
	  border-top-left-radius: 0;
	  border-top-right-radius: 0;
	}
}


</style>
</head>
<%
String str=(String)request.getParameter("path");

if(str==null){
	str=(String)request.getAttribute("path");
}
//setting default
if(str==null){
	str="Homepage";
}
String error=(String)request.getAttribute("auth");
String status=(String)request.getAttribute("status");

if(status==null){
	status=(String)request.getParameter("status");
}
if(status!=null && status.equals("login")){
	status="Please Login first!";
}
if(error==null && status==null){
	status=" ";
}
if(status==null){
	status=" ";
}
%>
<body style="background-color:#d3d1d1">
<div class="container-fluid">
<div class="pageheader" style="padding:10px 10px 10px 10px;background-color: #ffffff;border-radius: 5px" >
<img src="${pageContext.request.contextPath}/logo.PNG" />
</div>
</div>
<center>

<div class="wrapper">
<div style="color: red"><%= status %></div>
    <form class="form-signin" method="POST" action="/arena/authenticate">       
      <h2 class="form-signin-heading">Please login</h2>
      <input type="text" class="form-control" name="username" placeholder="Email Address" required="" autofocus="" /><br>
      <input type="password" class="form-control" name="password" placeholder="Password" required=""/> <br>
      <input type="hidden" name="return_path" value="<%= str %>" />     
      <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>   
    </form>
  </div>
  Not a User Yet <a href="/arena/Register_page.jsp">Register Here</a>
</center>
</body>
</html>