<%@ page import="classes.Homepage_Utility"%>
<%@ page import="classes.Video"%>
<%@ page import="javax.servlet.*"%>
<%@ page import="javax.servlet.http.*"%>
<%@ page import="java.util.Date" %>
<%@ page session="false"%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/bootstrap.min.css" /> 
</head>
<body style="background-color:#d3d1d1">
<div class="container-fluid">
<div class="pageheader" style="padding:10px 10px 10px 10px;background-color: #ffffff;border-radius: 5px" >

<img src="${pageContext.request.contextPath}/logo.PNG" />
<div style="float: right">
<a href="${pageContext.request.contextPath}/upload.jsp"><img src="${pageContext.request.contextPath}/upload.png"/></a>
<%
HttpSession hs=request.getSession(false);
if(hs==null){
	System.out.println("New User without session");
	%><a href="${pageContext.request.contextPath}/Login_page.jsp?path=Homepage">Login</a><%
}
else{
%> Hello,<%= hs.getAttribute("name")%><%
	System.out.println("Session recovered"+ new Date(hs.getCreationTime()));
}
%>
</div>

</div>
<hr>
<div class="container" style="padding-bottom: 5px">
<% Homepage_Utility ob=new Homepage_Utility();
Video array[]=ob.get_data();
//System.out.println(array[3].video_id);
int count=0;
for(int row=0;count<array.length;row++){
int inner_count=0;
%>
<div class="row">
<% for(int i=row*5;inner_count<5 && i<array.length;i++,inner_count++){
%>
<div class="card" style="width: 12rem;margin-left: 4px;margin-top: 2px">
  <img class="card-img-top" style="height:100" src="${pageContext.request.contextPath}/images/<%= array[i].video_id %>.jpg" alt="Card image cap">

  <div class="card-block">
  <a href="${pageContext.request.contextPath}/temp?id=<%= array[i].video_id%>">
    <h7 class="card-title"><%= array[i].video_title%></h7></a></br>
   
   <small><%= array[i].video_description%>...</small>
   <small class="text-muted">Uploaded by:<%= array[i].video_author %></small>
   </div>
</div>

<% count++; } %>
</div>
<% } %>
</div>
</body>
</html>